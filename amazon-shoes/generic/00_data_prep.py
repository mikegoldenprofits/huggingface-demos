import datasets
from datasets import load_dataset

print(datasets.__version__)


dataset = load_dataset("amazon_us_reviews", "Shoes_v1_00", split="train[:1%]")
print(dataset.shape)
dataset = dataset.remove_columns(
    [
        "marketplace",
        "customer_id",
        "review_id",
        "product_id",
        "product_parent",
        "product_title",
        "product_category",
        "helpful_votes",
        "total_votes",
        "vine",
        "verified_purchase",
        "review_headline",
        "review_date",
    ]
)


def decrement_stars(row):
    return {"star_rating": row["star_rating"] - 1}


dataset = dataset.map(decrement_stars)
dataset = dataset.rename_column("star_rating", "labels")
dataset = dataset.rename_column("review_body", "text")
dataset_split = dataset.train_test_split(test_size=0.1, shuffle=True, seed=59)

dataset_split["train"].save_to_disk("data/amazon_shoe_reviews_train")
dataset_split["test"].save_to_disk("data/amazon_shoe_reviews_test")
